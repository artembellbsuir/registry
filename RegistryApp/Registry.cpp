#include "Registry.h"

#include <iostream>
#include <winreg.h>
#include <winerror.h>
#include <tchar.h>
#include <AclAPI.h>
#include <sddl.h>


#define MAX_KEY_LENGTH 255

BOOL Registry::CreateRegistryKey(
	HKEY hKeyParent,
	const std::wstring& subkey
) {
	DWORD dwDisposition;
	HKEY  hKey;
	LSTATUS result;

	// Creates the specified registry key. If the key already exists, the function opens it
	result = ::RegCreateKeyEx(
		hKeyParent,					// A handle to an open registry key. 
		subkey.c_str(),				// The name of a subkey that this function opens or creates. 
		0,							// This parameter is reserved and must be zero.
		NULL,						// The user-defined class type of this key. This parameter may be ignored. This parameter can be NULL.
		REG_OPTION_NON_VOLATILE,	// This key is not volatile; this is the default.
		KEY_CREATE_SUB_KEY,			// A mask that specifies the access rights for the key to be created
		NULL,						// A pointer to a SECURITY_ATTRIBUTES structure that determines whether the returned handle can be inherited by child processes.If lpSecurityAttributes is NULL, the handle cannot be inherited.
		&hKey,						// A pointer to a variable that receives a handle to the opened or created key.
		&dwDisposition				// A pointer to a variable that receives one of the following disposition values. If lpdwDisposition is NULL, no disposition information is returned.
	);

	if (ERROR_SUCCESS != result)
	{
		std::cout << "Error opening or creating new key." << std::endl;
		::RegCloseKey(hKey);
		return FALSE;
	}

	if (REG_OPENED_EXISTING_KEY == dwDisposition) {
		std::cout << "This key is already exists." << std::endl;
	}

	::RegCloseKey(hKey);
	return TRUE;
}

BOOL Registry::WriteDword(
	HKEY hKeyParent,
	const std::wstring& subkey,
	const std::wstring& valueName,
	DWORD data
) {
	LSTATUS result;
	HKEY hKey;

	// Opens the specified registry key.
	result = ::RegOpenKeyEx(
		hKeyParent,		// A handle to an open registry key.
		subkey.c_str(),	// The name of the registry subkey to be opened.
		0,				// Specifies the option to apply when opening the key.
		KEY_WRITE,		// A mask that specifies the desired access rights to the key to be opened.
		&hKey			// A pointer to a variable that receives a handle to the opened key.
	);

	if (ERROR_SUCCESS == result)
	{
		// Sets the data and type of a specified value under a registry key.
		result = ::RegSetValueEx(
			hKey,									// A handle to an open registry key.	
			valueName.c_str(),						// The name of the value to be set. If a value with this name is not already present in the key, the function adds it to the key.
			0,										// This parameter is reserved and must be zero.
			REG_DWORD,								// The type of data pointed to by the lpData parameter. 
			(LPBYTE)(&data),
			//reinterpret_cast<const BYTE*>(&data),	// The data to be stored.
			sizeof(data)							// The size of the information pointed to by the lpData parameter, in bytes.
		);

		if (ERROR_SUCCESS == result)
		{
			::RegCloseKey(hKey);
			return TRUE;
		}
		std::cout << "Error adding new dword value to the key." << std::endl;
		::RegCloseKey(hKey);
		return FALSE;
	}
	std::cout << "Error opening the key." << std::endl;
	return FALSE;
}

BOOL Registry::WriteString(
	HKEY hKeyParent,
	const std::wstring& subkey,
	const std::wstring& valueName,
	const std::wstring& data
)
{
	LSTATUS result;
	HKEY hKey;

	result = ::RegOpenKeyEx(
		hKeyParent,
		subkey.c_str(),
		0,
		KEY_WRITE,
		&hKey
	);

	if (ERROR_SUCCESS == result)
	{
		result = ::RegSetValueEx(
			hKey,
			valueName.c_str(),
			0,
			REG_SZ,
			(LPBYTE)(data.c_str()),
			((data.length() + 1) * sizeof(wchar_t))
		);

		if (ERROR_SUCCESS == result)
		{
			::RegCloseKey(hKey);
			return TRUE;
		}
		std::cout << "Error adding new string value to the key." << std::endl;
		::RegCloseKey(hKey);
		return FALSE;
	}
	std::cout << "Error opening the key." << std::endl;
	return FALSE;
}

BOOL Registry::ReadDword(
	HKEY hKeyParent,
	const std::wstring& subkey,
	const std::wstring& valueName,
	DWORD* dwReadData
) {
	HKEY hKey;
	LSTATUS result;

	result = ::RegOpenKeyEx(
		hKeyParent,
		subkey.c_str(),
		0,
		KEY_READ,
		&hKey
	);

	if (ERROR_SUCCESS == result)
	{
		DWORD data;
		DWORD readDataLen = sizeof(DWORD);

		result = ::RegQueryValueEx(
			hKey,				// A handle to an open registry key.
			valueName.c_str(),	// The name of the registry value.
			NULL,				// This parameter is reserved and must be NULL.
			NULL,				// A pointer to a variable that receives a code indicating the type of data stored in the specified value. The lpType parameter can be NULL if the type code is not required.
			(LPBYTE)(&data),	// A pointer to a buffer that receives the value's data. 
			&readDataLen				// A pointer to a variable that specifies the size of the buffer pointed to by the lpData parameter, in bytes.
		);

		// TODO: add every where RegGetValue to ensure it sz string
		if (ERROR_SUCCESS == result && readDataLen == sizeof(DWORD))
		{
			(*dwReadData) = data;
			::RegCloseKey(hKey);
			return TRUE;
		}
		std::cout << "Error reading dword value of the key." << std::endl;
		::RegCloseKey(hKey);
		return FALSE;
	}

	std::cout << "Error opening the key." << std::endl;
	return FALSE;
}

BOOL Registry::ReadString(
	HKEY hKeyParent,
	const std::wstring& subkey,
	const std::wstring& valueName,
	std::wstring& readData
) {
	LSTATUS result;
	HKEY hKey;
	DWORD len = 1000;
	DWORD readDataLen = len;

	auto readBuffer = (PWCHAR)malloc(sizeof(PWCHAR) * len);

	if (NULL == readBuffer) {
		std::cout << "Error allocating memory for buffer." << std::endl;
		return FALSE;
	}

	result = ::RegOpenKeyEx(
		hKeyParent,
		subkey.c_str(),
		0,
		KEY_READ,
		&hKey
	);

	if (ERROR_SUCCESS == result)
	{
		result = RegQueryValueEx(
			hKey,
			valueName.c_str(),
			NULL,
			NULL,
			(LPBYTE)readBuffer,
			&readDataLen
		);

		while (ERROR_MORE_DATA == result)
		{
			len += 200;
			auto reallocatedBuffer = (PWCHAR)realloc(readBuffer, len);
			if (NULL == reallocatedBuffer) {
				std::cout << "Error reallocating memory for buffer." << std::endl;
				return FALSE;
			}

			readBuffer = reallocatedBuffer;

			readDataLen = len;
			result = RegQueryValueEx(
				hKey,
				valueName.c_str(),
				NULL,
				NULL,
				(BYTE*)readBuffer,
				&readDataLen
			);
		}


		if (ERROR_SUCCESS == result)
		{
			std::wstring arr(readBuffer);
			readData = arr;
			::RegCloseKey(hKey);
			return TRUE;
		}
		std::cout << "Error reading string value of the key." << std::endl;
		::RegCloseKey(hKey);
		return FALSE;
	}
	std::cout << "Error opening the key." << std::endl;
	return FALSE;
}

std::wstring Registry::FindSubkey(
	HKEY hParentKey,
	const std::wstring& subkey,
	const std::wstring acc
) {
	DWORD subkeysAmount;
	DWORD maxSubkeyLen, currentSubkeyLen;
	DWORD result;

	DWORD cSubKeys = 0;
	TCHAR    achKey[MAX_KEY_LENGTH];
	DWORD    cbMaxSubKey;              // longest subkey size 

	int retCode = ::RegQueryInfoKey(
		hParentKey,     // A handle to an open registry key. 
		NULL,           // A pointer to a buffer that receives the user-defined class of the key. This parameter can be NULL.
		0,				// A pointer to a variable that specifies the size of the buffer pointed to by the lpClass parameter, in characters.
		NULL,           // This parameter is reserved and must be NULL.
		&cSubKeys,      // A pointer to a variable that receives the number of subkeys that are contained by the specified key.
		&cbMaxSubKey,   // A pointer to a variable that receives the size of the key's subkey with the longest name, in Unicode characters, not including the terminating null character.
		NULL,           // A pointer to a variable that receives the size of the longest string that specifies a subkey class, in Unicode characters. This parameter can be NULL.
		NULL,           // A pointer to a variable that receives the number of values that are associated with the key. This parameter can be NULL.
		NULL,           // A pointer to a variable that receives the size of the key's longest value name, in Unicode characters. This parameter can be NULL.
		NULL,			// A pointer to a variable that receives the size of the longest data component among the key's values, in bytes. This parameter can be NULL.
		NULL,			// A pointer to a variable that receives the size of the key's security descriptor, in bytes. This parameter can be NULL.
		NULL			// A pointer to a FILETIME structure that receives the last write time. This parameter can be NULL.
	);


	if (cSubKeys)
	{

		DWORD    cbName;
		std::wstring s;

		for (int i = 0; i < cSubKeys; i++)
		{
			cbName = MAX_KEY_LENGTH;
			// Enumerates the subkeys of the specified open registry key. The function retrieves information about one subkey each time it is called.
			retCode = ::RegEnumKeyEx(
				hParentKey,	// A handle to an open registry key. 
				i,			// The index of the subkey to retrieve. 
				achKey,		// A pointer to a buffer that receives the name of the subkey, including the terminating null character. 
				&cbName,	// A pointer to a variable that specifies the size of the buffer specified by the lpName parameter, in characters. 
				NULL,		// This parameter is reserved and must be NULL.
				NULL,		// A pointer to a buffer that receives the user-defined class of the enumerated subkey. This parameter can be NULL.
				NULL,		// A pointer to a variable that specifies the size of the buffer specified by the lpClass parameter, in characters. This parameter can be NULL only if lpClass is NULL.
				NULL		// A pointer to FILETIME structure that receives the time at which the enumerated subkey was last written. This parameter can be NULL.
			);

			if (retCode == ERROR_SUCCESS)
			{
				std::wstring currentKey(achKey);
				if (currentKey == subkey) {
					return acc + currentKey;
					/*return TRUE;*/
				}

				HKEY innerKey;
				result = ::RegOpenKeyEx(
					hParentKey,
					currentKey.c_str(),
					0,
					KEY_READ,
					&innerKey
				);

				if (ERROR_SUCCESS == result) {
					std::wstring newAcc = acc + currentKey + L"\\\\";
					std::wstring res = Registry::FindSubkey(
						innerKey,
						subkey,
						newAcc
					);

					if (res != L"") {
						::RegCloseKey(innerKey);
						return res;
						//return TRUE;
					}
				}

				RegCloseKey(innerKey);
			}
		}
	}
	return L"";
}

std::string GetAceFlagValueString(
	std::string param
) {
	if ("CI" == param) return "(SDDL_CONTAINER_INHERIT)";
	if ("OI" == param) return "(SDDL_OBJECT_INHERIT)";
	if ("NP" == param) return "(SDDL_NO_PROPAGATE)";
	if ("IO" == param) return "(SDDL_INHERIT_ONLY)";
	if ("ID" == param) return "(SDDL_INHERITED)";
	if ("SA" == param) return "(SDDL_AUDIT_SUCCESS)";
	if ("FA" == param) return "(SDDL_AUDIT_FAILURE)";
	else return "UNKNOWN FLAG";
}


// make it BOOL
int ReadDaclAces(PACL pDacl)
{
	int result;
	LPVOID pAce = NULL;
	PACE_HEADER pAceheader = NULL;

	for (DWORD i = 0; i < pDacl->AceCount; i++)
	{
		result = ::GetAce(pDacl, i, &pAce); {
			if (result) {
				PACCESS_ALLOWED_ACE pAccessAllowedAce = (PACCESS_ALLOWED_ACE)pAce;
				pAccessAllowedAce->Mask;
			}
			else {
				std::cout << "Error reading ACE mask." << std::endl;
			}
		}
		
	}

	return 0;
}

void Registry::ReadKeyFlags(
	HKEY hKey
) {
	LSTATUS result;
	DWORD securityDescriptorSize;
	DWORD subkeysNumber;

	// here - for allocating memory (we need to get securityDescriptorSize)
	result = ::RegQueryInfoKey(
		hKey,
		NULL,
		0,
		NULL,
		&subkeysNumber,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		&securityDescriptorSize,
		NULL
	);

	if (ERROR_SUCCESS == result) {
		DWORD lcbSecurityDescriptor = 0;
		PSECURITY_DESCRIPTOR pSecurityDescriptor = (PSECURITY_DESCRIPTOR)malloc(lcbSecurityDescriptor);

		result = ::RegGetKeySecurity(
			hKey,
			DACL_SECURITY_INFORMATION,
			pSecurityDescriptor,
			&securityDescriptorSize
		);

		if (ERROR_SUCCESS == result)
		{
			BOOL bDaclPresent = FALSE;
			BOOL bDaclDefaulted = FALSE;
			PACL pDacl = NULL;

			result = ::GetSecurityDescriptorDacl(
				pSecurityDescriptor,
				&bDaclPresent,
				&pDacl,
				&bDaclDefaulted
			);

			if (0 != result) {
				result = ::ReadDaclAces(pDacl);
				if (!result) {
					std::cout << "Error reading DACL ACEs values." << std::endl;
				}
			}
			else {
				std::cout << "Error getting key security descriptor." << std::endl;
			}
		}
		else {
			std::cout << "Error getting key security descriptor." << std::endl;
		}
	}
	else {
		std::cout << "Error getting key security descriptor size." << std::endl;
	}
}