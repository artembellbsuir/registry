﻿#include <iostream>
#include <Windows.h>
#include <winerror.h>
#include <string>
#include <tchar.h>

#include "Registry.h"

#define CMD_UNKNOWN 0
#define CMD_WRITE_DWORD 1
#define CMD_WRITE_STRING 2
#define CMD_READ_DWORD 3
#define CMD_READ_STRING 4

#define CMD_KEY_CREATE 10
#define	CMD_KEY_FIND 11
#define CMD_KEY_READ_SECURITY_FLAGS 12
#define CMD_EXIT 20


//Чтение флагов, составляющих маску ключа

int parseCommand() {
	std::string command;
	std::cin >> command;

	if ("create" == command) return CMD_KEY_CREATE;
	if ("write/dw" == command) return CMD_WRITE_DWORD;
	if ("write/str" == command) return CMD_WRITE_STRING;
	if ("read/dw" == command) return CMD_READ_DWORD;
	if ("read/str" == command) return CMD_READ_STRING;
	if ("find" == command) return CMD_KEY_FIND;
	if ("read/f" == command) return CMD_KEY_READ_SECURITY_FLAGS;
	if ("exit" == command) return CMD_EXIT;
	else return CMD_UNKNOWN;
}

int main()
{
	BOOL result;
	std::wstring subkey, valueName, strData;
	DWORD dwData;


	int command = -1;

	while (CMD_EXIT != command) {
		std::cout << "Enter cmd:" << std::endl;
		command = ::parseCommand();

		switch (command) {
		case CMD_KEY_CREATE: {
			std::cout << "Enter subkey:" << std::endl;
			std::wcin >> subkey;
			result = Registry::CreateRegistryKey(HKEY_CURRENT_USER, subkey);
			break;
		}
		case CMD_WRITE_DWORD: {
			std::cout << "Enter subkey: ";
			std::wcin >> subkey;
			std::cout << "Enter value name: ";
			std::wcin >> valueName;

			std::cout << "Enter dword data: ";
			std::cin >> dwData;
			result = Registry::WriteDword(HKEY_CURRENT_USER, subkey, valueName, dwData);
			break;
		}
		case CMD_WRITE_STRING: {
			std::cout << "Enter subkey: ";
			std::wcin >> subkey;
			std::cout << "Enter value name: ";
			std::wcin >> valueName;
			std::cout << "Enter string data: ";
			std::wcin >> strData;
			result = Registry::WriteString(HKEY_CURRENT_USER, subkey, valueName, strData);
			break;
		}
		case CMD_READ_DWORD: {
			std::cout << "Enter subkey: ";
			std::wcin >> subkey;
			std::cout << "Enter value name: ";
			std::wcin >> valueName;
			DWORD dwData;
			result = Registry::ReadDword(HKEY_CURRENT_USER, subkey, valueName, &dwData);
			std::cout << "Result dword data: " << dwData << std::endl;
			break;
		}
		case CMD_READ_STRING: {
			std::cout << "Enter subkey: ";
			std::wcin >> subkey;
			std::cout << "Enter value name: ";
			std::wcin >> valueName;
			std::wstring strData1;
			result = Registry::ReadString(HKEY_CURRENT_USER, subkey, valueName, strData1);
			std::wcout << "Result string data: " << strData1 << std::endl;
			break;
		}
		case CMD_KEY_FIND: {
			std::cout << "Enter subkey: ";
			std::wcin >> subkey;
			std::wstring accumulator;
			std::wstring result = Registry::FindSubkey(HKEY_CURRENT_USER, subkey, accumulator);
			result != L""
				? std::wcout << "Key found: " << result << std::endl
				: std::wcout << "Key NOT found." << std::endl;

			//std::wcout << "Result string data: " << strData1 << std::endl;
			break;
		}
		case CMD_KEY_READ_SECURITY_FLAGS: {
			HKEY currentKey;
			//printf("Enter the key name: ");

			//scanf("%s", buffer);
			std::wstring str;
			//std::wcin >> str;

			DWORD dwResult = RegOpenKey(HKEY_CURRENT_USER, L"beewere\\dw1", &currentKey);
			if (dwResult != ERROR_SUCCESS)
			{
				printf("Can not open key\n");
			}
			else
			{
				Registry::ReadKeyFlags(currentKey);
				RegCloseKey(currentKey);
			}
			break;
		}
		case CMD_EXIT: {
			break;
		}
		default: {
			std::cout << "You have entered unknown command. Try again." << std::endl;
		}
		}
	}

}
