#pragma once

#include <Windows.h>
#include <string>

namespace Registry {
	BOOL CreateRegistryKey(
		HKEY hKeyParent,
		const std::wstring& subkey
	);

	BOOL WriteDword(
		HKEY hKeyParent,
		const std::wstring& subkey,
		const std::wstring& valueName,
		DWORD data
	);

	BOOL WriteString(
		HKEY hKeyParent,
		const std::wstring& subkey,
		const std::wstring& valueName,
		const std::wstring& data
	);

	BOOL ReadDword(
		HKEY hKeyParent,
		const std::wstring& subkey,
		const std::wstring& valueName,
		DWORD* dwReadData
	);

	BOOL ReadString(
		HKEY hKeyParent,
		const std::wstring& subkey,
		const std::wstring& valueName,
		std::wstring& readData
	);

	std::wstring FindSubkey(
		HKEY hParentKey,
		const std::wstring& subkey,
		const std::wstring acc
	);

	void ReadKeyFlags(HKEY currentKey);

};